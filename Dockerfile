# build twitch CLI in a way that works on both AMD64 and ARM64
FROM golang AS builder

RUN git clone https://github.com/twitchdev/twitch-cli \
 && cd twitch-cli \
 && go build --ldflags "-s -w -X main.buildVersion=source"

# download latest binary of the webhook tool
FROM buildpack-deps:curl AS webhook-dl

RUN dpkg --print-architecture

WORKDIR /home/
RUN wget https://github.com/adnanh/webhook/releases/download/2.8.0/webhook-linux-$(dpkg --print-architecture).tar.gz \
 && tar -xf webhook-linux-$(dpkg --print-architecture).tar.gz \
 && mv webhook-linux-$(dpkg --print-architecture)/webhook /bin/webhook \
 && chmod u+x /bin/webhook

# release container
FROM debian:bullseye-slim

RUN set -eux\
 && apt-get update\
 && apt-get install -y --no-install-recommends ca-certificates\
 && rm -rf /var/lib/apt/lists/*

COPY --from=builder /go/twitch-cli/twitch-cli /bin/twitch
COPY --from=webhook-dl /bin/webhook /bin/webhook
COPY entrypoint.sh /bin/entrypoint.sh

RUN chmod u+x /bin/entrypoint.sh \
 && mkdir /hook # hook.yaml configuration file will be expected in this folder

# make sure that the programs actually work
RUN twitch version\
 && webhook -version

ENTRYPOINT ["entrypoint.sh"]
CMD ["mock-api", "start"]
